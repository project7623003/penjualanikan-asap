package com.java.penjualan.ikanasap.dto;


import com.java.penjualan.ikanasap.model.Fish;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SellResponse {

    private Integer id;

    private LocalDateTime createdDate;

    private Fish fish;

    private Float price;

    private Float change;
}
