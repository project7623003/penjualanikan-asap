package com.java.penjualan.ikanasap.controller;

import com.java.penjualan.ikanasap.dto.CreateFishRequest;
import com.java.penjualan.ikanasap.dto.FishResponse;
import com.java.penjualan.ikanasap.dto.UpdateFishRequest;
import com.java.penjualan.ikanasap.dto.WebResponse;
import com.java.penjualan.ikanasap.model.Fish;
import com.java.penjualan.ikanasap.service.FishService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/fish")
public class FishController {


    private final FishService fishService;

    public FishController(FishService fishService) {
        this.fishService = fishService;
    }

    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public WebResponse<FishResponse> create(@RequestBody CreateFishRequest request){

        FishResponse response = fishService.create(request);
        return WebResponse.<FishResponse>builder().data(response).build();
    }

    @PutMapping(value = "/update",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public WebResponse<FishResponse> update(@RequestBody UpdateFishRequest request){
        FishResponse response = fishService.update(request);
        return WebResponse.<FishResponse>builder().data(response).build();
    }

    @GetMapping(value = "/{fishId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public WebResponse<FishResponse> get(@PathVariable("fishId") Integer fishId){
        FishResponse response = fishService.get(fishId);
        return WebResponse.<FishResponse>builder().data(response).build();
    }

    @DeleteMapping("/{fishId}")
    public WebResponse<String> delete(@PathVariable("fishId") Integer fishId){
        fishService.delete(fishId);
        return WebResponse.<String>builder().data("Success delete").build();
    }

    @PostMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public WebResponse<List<FishResponse>> getAll(){
        List<FishResponse> list = fishService.getAll();
        return WebResponse.<List<FishResponse>>builder().data(list).build();
    }
}
