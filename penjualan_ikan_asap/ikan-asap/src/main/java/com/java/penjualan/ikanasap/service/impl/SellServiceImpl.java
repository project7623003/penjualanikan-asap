package com.java.penjualan.ikanasap.service.impl;

import com.java.penjualan.ikanasap.dto.SearchSellingRequest;
import com.java.penjualan.ikanasap.dto.SellRequest;
import com.java.penjualan.ikanasap.dto.SellResponse;
import com.java.penjualan.ikanasap.model.Fish;
import com.java.penjualan.ikanasap.model.Selling;
import com.java.penjualan.ikanasap.repository.FishRepository;
import com.java.penjualan.ikanasap.repository.SellRepository;
import com.java.penjualan.ikanasap.service.SellService;
import jakarta.persistence.criteria.Predicate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class SellServiceImpl implements SellService {

    private final SellRepository sellRepository;

    private final FishRepository fishRepository;


    public SellServiceImpl(SellRepository sellRepository, FishRepository fishRepository) {
        this.sellRepository = sellRepository;
        this.fishRepository = fishRepository;
    }

    @Override
    public SellResponse trx(SellRequest request) {

        //check fish
        Optional<Fish> fish = fishRepository.findByName(request.getFishName());
        if(fish.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Data already exist");
        }

        Selling selling = new Selling();
        selling.setFish(fish.get());
        selling.setPrice(request.getPrice());
        selling.setChange(request.getChange());
        sellRepository.save(selling);

        SellResponse response = new SellResponse();
        response.setId(selling.getId());
        response.setChange(selling.getChange());
        response.setPrice(selling.getPrice());
        response.setFish(selling.getFish());
        response.setCreatedDate(selling.getCreatedDate());
        return response;
    }

    @Override
    public SellResponse get(Integer sellId) {

        //check data
        Optional<Selling> sell = sellRepository.findById(sellId);
        if(sell.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Data not found");
        }

        return SellResponse.builder()
                .createdDate(sell.get().getCreatedDate())
                .change(sell.get().getChange())
                .price(sell.get().getPrice())
                .fish(sell.get().getFish()).build();
    }

    @Override
    public void delete(Integer sellId) {
        Optional<Selling> sell = sellRepository.findById(sellId);
        if(sell.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Data not found");
        }

        sellRepository.delete(sell.get());
    }

    @Override
    public List<SellResponse> getAll() {
        List<Selling> list = sellRepository.findAll();
        return list.stream().map(x -> toSellResponse(x)).toList();
    }

    @Override
    public Page<SellResponse> search(SearchSellingRequest request) {

        Specification<Selling> specification = (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if(Objects.nonNull(request.getPrice())){
                predicates.add(criteriaBuilder.or(
                        criteriaBuilder.equal(root.get("price"), request.getPrice())
                ));
            }
            return query.where(predicates.toArray(new Predicate[]{})).getRestriction();
        };

        Pageable pageable = PageRequest.of(request.getPage(), request.getSize());
        Page<Selling> pages = sellRepository.findAll(specification,pageable);
        List<SellResponse> list = pages.getContent().stream().map(this::toSellResponse).collect(Collectors.toList());

        return new PageImpl<>(list, pageable, pages.getTotalElements());
    }

    private SellResponse toSellResponse(Selling selling){
        return SellResponse.builder()
                .id(selling.getId())
                .fish(selling.getFish())
                .price(selling.getPrice())
                .change(selling.getChange())
                .createdDate(selling.getCreatedDate()).build();
    }

}
