package com.java.penjualan.ikanasap.controller;


import com.java.penjualan.ikanasap.dto.CreatePriceRequest;
import com.java.penjualan.ikanasap.dto.PriceResponse;
import com.java.penjualan.ikanasap.dto.UpdatePriceRequest;
import com.java.penjualan.ikanasap.dto.WebResponse;
import com.java.penjualan.ikanasap.service.PriceService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/price")
public class PriceController {

    private final PriceService priceService;

    public PriceController(PriceService priceService) {
        this.priceService = priceService;
    }

    @PostMapping(value = "/create",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public WebResponse<PriceResponse> create(@RequestBody CreatePriceRequest request){
        PriceResponse response = priceService.create(request);
        return WebResponse.<PriceResponse>builder().data(response).build();
    }

    @PutMapping(value = "/update",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public WebResponse<PriceResponse> update(@RequestBody UpdatePriceRequest request){
        PriceResponse response = priceService.update(request);
        return WebResponse.<PriceResponse>builder().data(response).build();
    }

    @GetMapping(value = "/{priceId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public WebResponse<PriceResponse> get(@PathVariable("priceId") Integer priceId){
        PriceResponse response = priceService.get(priceId);
        return WebResponse.<PriceResponse>builder().data(response).build();
    }

    @DeleteMapping("/{priceId}")
    public WebResponse<String> delete(@PathVariable("priceId") Integer priceId){
        priceService.delete(priceId);
        return WebResponse.<String>builder().data("Success delete").build();
    }

    @PostMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public WebResponse<List<PriceResponse>> getAll(){
        List<PriceResponse> responses = priceService.getAll();
        return WebResponse.<List<PriceResponse>>builder().data(responses).build();
    }
}
