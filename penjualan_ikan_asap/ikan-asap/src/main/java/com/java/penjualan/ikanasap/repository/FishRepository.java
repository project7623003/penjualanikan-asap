package com.java.penjualan.ikanasap.repository;

import com.java.penjualan.ikanasap.model.Fish;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface FishRepository extends JpaRepository<Fish, Integer> {

    @Query("SELECT a FROM Fish a WHERE a.name= :name")
    public Optional<Fish> findByName(@Param("name") String name);


}
