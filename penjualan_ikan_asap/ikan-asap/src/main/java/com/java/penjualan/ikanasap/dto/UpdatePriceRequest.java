package com.java.penjualan.ikanasap.dto;


import com.java.penjualan.ikanasap.model.Fish;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UpdatePriceRequest {

    private Integer id;

    private Integer fish;

    private Float price;
}
