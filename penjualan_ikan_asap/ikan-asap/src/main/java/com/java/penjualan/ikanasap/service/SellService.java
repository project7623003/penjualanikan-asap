package com.java.penjualan.ikanasap.service;


import com.java.penjualan.ikanasap.dto.SearchSellingRequest;
import com.java.penjualan.ikanasap.dto.SellRequest;
import com.java.penjualan.ikanasap.dto.SellResponse;
import com.java.penjualan.ikanasap.model.Selling;
import org.springframework.data.domain.Page;

import java.util.List;

public interface SellService {

    SellResponse trx(SellRequest request);

    SellResponse get(Integer sellId);

    void delete(Integer sellId);

    List<SellResponse> getAll();

    Page<SellResponse> search(SearchSellingRequest request);
}
