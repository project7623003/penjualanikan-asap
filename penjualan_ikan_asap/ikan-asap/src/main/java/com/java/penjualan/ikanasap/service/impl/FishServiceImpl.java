package com.java.penjualan.ikanasap.service.impl;

import com.java.penjualan.ikanasap.dto.CreateFishRequest;
import com.java.penjualan.ikanasap.dto.FishResponse;
import com.java.penjualan.ikanasap.dto.UpdateFishRequest;
import com.java.penjualan.ikanasap.model.Fish;
import com.java.penjualan.ikanasap.repository.FishRepository;
import com.java.penjualan.ikanasap.service.FishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class FishServiceImpl implements FishService {

    @Autowired
    private FishRepository fishRepository;

    @Override
    public Optional<Fish> findByName(String name) {
        return fishRepository.findByName(name);
    }

    @Override
    public FishResponse create(CreateFishRequest request) {

        //check name duplicate
        Optional<Fish> fishName = this.findByName(request.getName());
        if(fishName.isPresent()){
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Data already exist");
        }

        Fish fish = new Fish();
        fish.setName(request.getName());
        fishRepository.save(fish);

        FishResponse response = new FishResponse();
        response.setName(fish.getName());
        response.setId(fish.getId());

        return response;
    }

    @Override
    public FishResponse update(UpdateFishRequest request) {

        //check old data
        Optional<Fish> oldData = fishRepository.findById(request.getId());
        if(oldData.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Data not found");
        }

        //check duplicate data
        Optional<Fish> name = this.findByName(request.getName());
        if(name.isPresent()){
            if(name.get().getId() != oldData.get().getId()){
                throw new ResponseStatusException(HttpStatus.CONFLICT, "Data already exist");
            }
        }

        oldData.get().setName(request.getName());
        fishRepository.save(oldData.get());

        FishResponse response = new FishResponse();
        response.setId(oldData.get().getId());
        response.setName(oldData.get().getName());
        return response;
    }

    @Override
    public FishResponse get(Integer fishId) {
        Fish fish = fishRepository.findById(fishId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Data not found"));

        return FishResponse.builder()
                .id(fish.getId())
                .name(fish.getName())
                .build();
    }

    @Override
    public List<FishResponse> getAll() {
        List<Fish> fishAll = fishRepository.findAll();
        return fishAll.stream().map(x -> toFishResponse(x)).toList();
    }

    @Override
    public void delete(Integer fishId) {
        Fish fish = fishRepository.findById(fishId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Data not found"));

        fishRepository.delete(fish);
    }

    private FishResponse toFishResponse(Fish fish){
        return FishResponse.builder()
                .id(fish.getId())
                .name(fish.getName()).build();
    }
}
