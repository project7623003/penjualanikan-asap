package com.java.penjualan.ikanasap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IkanAsapApplication {

	public static void main(String[] args) {
		SpringApplication.run(IkanAsapApplication.class, args);
	}

}
