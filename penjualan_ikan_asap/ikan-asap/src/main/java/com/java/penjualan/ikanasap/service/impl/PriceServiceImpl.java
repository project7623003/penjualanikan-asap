package com.java.penjualan.ikanasap.service.impl;

import com.java.penjualan.ikanasap.dto.CreatePriceRequest;
import com.java.penjualan.ikanasap.dto.FishResponse;
import com.java.penjualan.ikanasap.dto.PriceResponse;
import com.java.penjualan.ikanasap.dto.UpdatePriceRequest;
import com.java.penjualan.ikanasap.model.Fish;
import com.java.penjualan.ikanasap.model.Price;
import com.java.penjualan.ikanasap.repository.FishRepository;
import com.java.penjualan.ikanasap.repository.PriceRepository;
import com.java.penjualan.ikanasap.service.PriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Service
public class PriceServiceImpl implements PriceService {

    @Autowired
    private PriceRepository priceRepository;

    @Autowired
    private FishRepository fishRepository;

    @Override
    public PriceResponse create(CreatePriceRequest request) {
        Price fish = this.findByFishId(request.getFish());

        if(fish != null){
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Data already exist");
        }

        Optional<Fish> fishData = fishRepository.findById(request.getFish());
        if(fishData.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Data not found");
        }

        Price price = new Price();
        price.setPrice(request.getPrice());
        price.setFish(fishData.get());
        priceRepository.save(price);

        PriceResponse response = new PriceResponse();
        response.setId(price.getId());
        response.setPrice(price.getPrice());
        response.setFish(price.getFish());
        return response;
    }

    @Override
    public Price findByFishId(Integer fishId) {
        return priceRepository.findByFishId(fishId);
    }

    @Override
    public PriceResponse update(UpdatePriceRequest request) {

        //check old data
        Optional<Price> oldData = priceRepository.findById(request.getId());
        if(oldData.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Data not found");
        }

        Price fishData = priceRepository.findByFishId(request.getFish());
        if(fishData != null){
            if(oldData.get().getId() != fishData.getId()){
                throw new ResponseStatusException(HttpStatus.CONFLICT, "Data already exist");
            }
        }

        Optional<Fish> fish = fishRepository.findById(request.getFish());
        if(fish.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Fish data not found");
        }

        oldData.get().setPrice(request.getPrice());
        oldData.get().setFish(fish.get());
        priceRepository.save(oldData.get());


        PriceResponse response = new PriceResponse();
        response.setId(oldData.get().getId());
        response.setPrice(oldData.get().getPrice());
        response.setFish(oldData.get().getFish());

        return response;
    }

    @Override
    public PriceResponse get(Integer priceId){

        //check id
        Optional<Price> price = priceRepository.findById(priceId);
        if(price.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Data not found");
        }

        return PriceResponse.builder()
                .id(price.get().getId())
                .price(price.get().getPrice())
                .fish(price.get().getFish())
                .build();
    }

    @Override
    public void delete(Integer priceId) {
       Optional<Price> price = priceRepository.findById(priceId);
       if(price.isEmpty()){
           throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Data not found");
       }

       priceRepository.delete(price.get());
    }

    @Override
    public List<PriceResponse> getAll() {
        List<Price> list = priceRepository.findAll();
        return list.stream().map(x -> toPriceResponse(x)).toList();
    }

    private PriceResponse toPriceResponse(Price price){
        return PriceResponse.builder()
                .id(price.getId())
                .fish(price.getFish())
                .price(price.getPrice())
                .build();
    }
}
