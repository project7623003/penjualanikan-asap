package com.java.penjualan.ikanasap.repository;


import com.java.penjualan.ikanasap.model.Price;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PriceRepository extends JpaRepository<Price, Integer> {

    @Query("SELECT a FROM Price a WHERE a.fish.id= :fishId")
    public Price findByFishId(@Param("fishId") Integer fishId);
}
