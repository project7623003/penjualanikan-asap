package com.java.penjualan.ikanasap.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SearchSellingRequest {

    private Float price;

    private Integer page;

    private Integer size;
}
