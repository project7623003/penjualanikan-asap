package com.java.penjualan.ikanasap.repository;

import com.java.penjualan.ikanasap.model.Selling;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SellRepository extends JpaRepository<Selling, Integer>, JpaSpecificationExecutor<Selling> {



}
