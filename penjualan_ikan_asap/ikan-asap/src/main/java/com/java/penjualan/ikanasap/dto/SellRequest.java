package com.java.penjualan.ikanasap.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SellRequest {

    private String fishName;

    private Float price;

    private Float change;
}
