package com.java.penjualan.ikanasap.service;

import com.java.penjualan.ikanasap.dto.*;
import com.java.penjualan.ikanasap.model.Price;

import java.util.List;

public interface PriceService {

    PriceResponse create(CreatePriceRequest request);

    Price findByFishId(Integer fishId);

    PriceResponse update(UpdatePriceRequest request);

    PriceResponse get(Integer priceId);

    void delete(Integer priceId);

    List<PriceResponse> getAll();
}
