package com.java.penjualan.ikanasap.service;

import com.java.penjualan.ikanasap.dto.CreateFishRequest;
import com.java.penjualan.ikanasap.dto.FishResponse;
import com.java.penjualan.ikanasap.dto.UpdateFishRequest;
import com.java.penjualan.ikanasap.model.Fish;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;


public interface FishService{

    Optional<Fish> findByName(@Param("name") String name);

    FishResponse create(CreateFishRequest request);

    FishResponse update(UpdateFishRequest request);

    FishResponse get(Integer fishId);

    List<FishResponse> getAll();

    void delete(Integer fishId);

}
