package com.java.penjualan.ikanasap.controller;


import com.java.penjualan.ikanasap.dto.*;
import com.java.penjualan.ikanasap.service.SellService;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/sell")
public class SellController {

    private final SellService service;


    public SellController(SellService service) {
        this.service = service;
    }

    @PostMapping(value = "/trx",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public WebResponse<SellResponse> trx(@RequestBody SellRequest request){
        SellResponse response = service.trx(request);
        return WebResponse.<SellResponse>builder().data(response).build();
    }

    @GetMapping(value = "/{sellId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public WebResponse<SellResponse> get(@PathVariable("sellId") Integer sellId){
        SellResponse response = service.get(sellId);
        return WebResponse.<SellResponse>builder().data(response).build();
    }

    @DeleteMapping("/{sellId}")
    public WebResponse<String> delete(@PathVariable("sellId") Integer sellId){
        service.delete(sellId);
        return WebResponse.<String>builder().data("Success delete").build();
    }

    @PostMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public WebResponse<List<SellResponse>> getAll(){
        List<SellResponse> list = service.getAll();
        return WebResponse.<List<SellResponse>>builder().data(list).build();
    }

    @PostMapping(value = "/search", produces = MediaType.APPLICATION_JSON_VALUE)
    public WebResponse<List<SellResponse>> search(@RequestParam(value = "price", required = false) Float price,
                                                               @RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
                                                               @RequestParam(value = "size", required = false, defaultValue = "10") Integer size){
        SearchSellingRequest request = new SearchSellingRequest();
        request.setPrice(price);
        request.setPage(page);
        request.setSize(size);

        Page<SellResponse> pageList = service.search(request);
        return WebResponse.<List<SellResponse>>builder()
                .data(pageList.getContent())
                .paging(PagingResponse.builder()
                        .currentPage(pageList.getNumber())
                        .size(pageList.getSize())
                        .totalPage(pageList.getTotalPages()).build())
                .build();
    }


}
