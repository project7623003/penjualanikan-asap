package com.java.penjualan.ikanasap.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.java.penjualan.ikanasap.dto.*;
import com.java.penjualan.ikanasap.model.Fish;
import com.java.penjualan.ikanasap.model.Price;
import com.java.penjualan.ikanasap.repository.FishRepository;
import com.java.penjualan.ikanasap.repository.PriceRepository;
import com.java.penjualan.ikanasap.service.FishService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class PriceControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    PriceRepository priceRepository;

    @Autowired
    FishRepository fishRepository;

    @Autowired
    private ObjectMapper objectMapper;


    @Test
    void createPriceSuccess() throws Exception{

        CreatePriceRequest request = new CreatePriceRequest();
        request.setPrice(5000F);
        request.setFish(1);

        mockMvc.perform(
                post("/price/create")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request))
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            WebResponse<PriceResponse> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

        });


    }

    @Test
    void updatePriceSuccess() throws Exception{
        Fish fish = new Fish();
        fish.setName("Ikan Kacang");
        fishRepository.save(fish);

        Price price = new Price();
        price.setFish(fish);
        price.setPrice(3000F);
        priceRepository.save(price);

        UpdatePriceRequest request = new UpdatePriceRequest();
        request.setId(1);
        request.setFish(1);
        request.setPrice(6000F);
        mockMvc.perform(
                put("/price/update")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request))
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            WebResponse<PriceResponse> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });


        });
    }

    @Test
    void getFishSuccess() throws Exception{
        Fish fish = new Fish();
        fish.setName("Ikan Kacang");
        fishRepository.save(fish);

        Price price = new Price();
        price.setPrice(5000F);
        price.setFish(fish);
        priceRepository.save(price);

        mockMvc.perform(
                get("/price/"+fish.getId())
                        .accept(MediaType.APPLICATION_JSON)
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            WebResponse<PriceResponse> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });
        });
    }

    @Test
    void getAllPrice() throws Exception{
        for(int i=0; i<2; i++){
            Fish fish = new Fish();
            fish.setName("Ikan Pari"+i);
            fishRepository.save(fish);

            Price price = new Price();
            price.setPrice(3000F);
            price.setFish(fish);
            priceRepository.save(price);
        }

        mockMvc.perform(
                post("/price/all")
                        .accept(MediaType.APPLICATION_JSON)
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            WebResponse<List<PriceResponse>> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });
        });
    }
}
