package com.java.penjualan.ikanasap.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.java.penjualan.ikanasap.dto.CreateFishRequest;
import com.java.penjualan.ikanasap.dto.FishResponse;
import com.java.penjualan.ikanasap.dto.UpdateFishRequest;
import com.java.penjualan.ikanasap.dto.WebResponse;
import com.java.penjualan.ikanasap.model.Fish;
import com.java.penjualan.ikanasap.repository.FishRepository;
import com.java.penjualan.ikanasap.service.FishService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class FishControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    FishRepository fishRepository;

    @Autowired
    FishService fishService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void createFishSuccess() throws Exception{

        CreateFishRequest request = new CreateFishRequest();
        request.setName("Ikan Tongkol");

        mockMvc.perform(
                post("/fish/create")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request))
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            WebResponse<FishResponse> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertEquals(request.getName(), response.getData().getName());
        });


    }

    @Test
    void updateFishSuccess() throws Exception{

        Fish fish = new Fish();
        fish.setName("Ikan Pari");
        fishRepository.save(fish);

        UpdateFishRequest request = new UpdateFishRequest();
        request.setId(1);
        request.setName("Ikan Tongkol");
        mockMvc.perform(
                put("/fish/update")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request))
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            WebResponse<FishResponse> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });


        });
    }

    @Test
    void getFishSuccess() throws Exception{
        Fish fish = new Fish();
        fish.setName("Ikan Pari");
        fishRepository.save(fish);

        mockMvc.perform(
                get("/fish/"+fish.getId())
                        .accept(MediaType.APPLICATION_JSON)
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            WebResponse<FishResponse> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });
        });
    }

    @Test
    void getAllFish() throws Exception{
        for(int i=0; i<2; i++){
            Fish fish = new Fish();
            fish.setName("Ikan Pari"+i);
            fishRepository.save(fish);
        }

        mockMvc.perform(
                post("/fish/all")
                        .accept(MediaType.APPLICATION_JSON)
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            WebResponse<List<FishResponse>> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });
        });
    }
}
