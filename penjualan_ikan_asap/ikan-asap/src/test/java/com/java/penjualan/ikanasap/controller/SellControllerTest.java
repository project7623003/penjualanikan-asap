package com.java.penjualan.ikanasap.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.java.penjualan.ikanasap.dto.*;
import com.java.penjualan.ikanasap.model.Fish;
import com.java.penjualan.ikanasap.model.Price;
import com.java.penjualan.ikanasap.model.Selling;
import com.java.penjualan.ikanasap.repository.FishRepository;
import com.java.penjualan.ikanasap.repository.PriceRepository;
import com.java.penjualan.ikanasap.repository.SellRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class SellControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    SellRepository sellRepository;

    @Autowired
    PriceRepository priceRepository;

    @Autowired
    FishRepository fishRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        Fish fish = new Fish();
        fish.setName("Ikan Pari");
        fishRepository.save(fish);

        Price price = new Price();
        price.setPrice(5000F);
        price.setFish(fish);
    }

    @Test
    void createTrxSuccess() throws Exception{

        SellRequest request = new SellRequest();
        request.setFishName("Ikan Pari");
        request.setPrice(5000F);
        request.setChange(0F);

        mockMvc.perform(
                post("/sell/trx")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request))
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            WebResponse<SellResponse> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

        });

    }

    @Test
    void getFishSuccess() throws Exception{
        Fish fish = new Fish();
        fish.setName("Ikan Kacang");
        fishRepository.save(fish);

        Price price = new Price();
        price.setPrice(5000F);
        price.setFish(fish);
        priceRepository.save(price);

        Selling sell = new Selling();
        sell.setChange(0F);
        sell.setFish(fish);
        sell.setPrice(5000F);
        sellRepository.save(sell);

        mockMvc.perform(
                get("/sell/"+sell.getId())
                        .accept(MediaType.APPLICATION_JSON)
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            WebResponse<SellResponse> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });
        });
    }

    @Test
    void getAllSell() throws Exception{
        for(int i=0; i<2; i++){
            Fish fish = new Fish();
            fish.setName("Ikan Pari"+i);
            fishRepository.save(fish);

            Selling selling = new Selling();
            selling.setFish(fish);
            selling.setPrice(3000F);
            selling.setChange(0F);
            sellRepository.save(selling);
        }

        mockMvc.perform(
                post("/sell/all")
                        .accept(MediaType.APPLICATION_JSON)
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            WebResponse<List<SellResponse>> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });
        });
    }

    @Test
    void searchSell() throws Exception{
        for(int i=0; i<2; i++){
            Fish fish = new Fish();
            fish.setName("Ikan Pari"+i);
            fishRepository.save(fish);

            Selling selling = new Selling();
            selling.setFish(fish);
            selling.setPrice(3000F);
            selling.setChange(0F);
            sellRepository.save(selling);
        }

        mockMvc.perform(
                post("/sell/search")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .queryParam("price", "3000F")
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            WebResponse<List<SellResponse>> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });
        });
    }
}
