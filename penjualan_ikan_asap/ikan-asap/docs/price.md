#Price API Spec
##Create price
- Endpoint : POST /price/create
- Request Body:
```json
{
  "price": 5000,
  "fish": 1
}
```

- Response Body:
```json
{
  "data": {
    "id": 1,
    "price": 5000,
    "fish": {
      "id": 1,
      "name": "Ikan Pari"
    }
  }
}
```

##Update price
- Endpoint : PUT /price/update
- Request Body :
```json
{
  "id": 1,
  "price": 5000,
  "fish": 1
}
```
-Response Body :
```json
{
  "data": {
    "id": 1,
    "price": 5000,
    "fish": {
      "id": 1,
      "name": "Ikan Pari"
    }
  }
}
```

##Get price
- Endpoint : GET /price/{priceId}
- Response Body :
```json
{
  "data": {
    "id": 1,
    "price": 5000,
    "fish": {
      "id": 1,
      "name": "Ikan Pari"
    }
  }
}
```

##DELETE price
- Endpoint : DELETE /price/{priceId}
- Response Body :
```json
{
  "data": "Success Delete"
}
```