#Fish API Spec
##Create fish
- Endpoint : POST /fish/create
- Request Body:
```json
{
  "name": "Ikan Pari"
}
```

- Response Body:
```json
{
  "data": {
    "id": 1,
    "name": "Ikan Pari"
  }
}
```

##Update Fish
- Endpoint : PUT /fish/update
- Request Body :
```json
{
  "id": 1,
  "name": "Ikan Cuwe"
}
```
-Response Body :
```json
{
  "data": {
    "id": 1,
    "name": "Ikan Cuwe"
  }
}
```

##Get Fish
- Endpoint : GET /fish/{fishId}
- Response Body :
```json
{
  "data": {
    "id": 1,
    "name": "Ikan Cuwe"
  }
}
```

##DELETE Fish
- Endpoint : DELETE /fish/{fishId}
- Response Body :
```json
{
  "data": "Success Delete"
}
```