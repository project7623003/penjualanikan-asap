#Sell API Spec
##Create trx
- Endpoint : POST /sell/trx
- Request Body:
```json
{
  "price": 5000,
  "fishName": "Ikan Cuwe",
  "change": 0
}
```

- Response Body:
```json
{
  "data": {
    "id": 1,
    "price": 5000,
    "change": 0,
    "fish": {
      "id": 1,
      "name": "Ikan Cuwe"
    },
    "createdDate": "timestamp"
  }
}
```

##Get sell
- Endpoint : GET /sell/{sellId}
- Response Body :
```json
{
  "data": {
    "id": 1,
    "price": 5000,
    "change": 0,
    "fish": {
      "id": 1,
      "name": "Ikan Cuwe"
    },
    "createdDate": "timestamp"
  }
}
```

##DELETE sell
- Endpoint : DELETE /trx/{priceId}
- Response Body :
```json
{
  "data": "Success Delete"
}
```